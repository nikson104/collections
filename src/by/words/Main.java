package by.words;

public class Main {

    public static void main(String[] args) {
        Words words = new Words();
        words.getText();
        words.convertToArray();
        words.convertToList();
        words.convertToSet();
        words.convertToMap();
        System.out.println(words.getList());
        System.out.println(words.getSet());
        System.out.println(words.getMap());
        try {
            words.findByWord();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
