package by.words;

import java.util.*;

public class Words implements Actions {
    static final Scanner SCANNER = new Scanner(System.in);
    private String text;
    private String[] words;
    private List<String> list;
    private Set<String> set;
    private HashMap<String, Integer> map = new HashMap<>();

    public String[] getWords() {
        return words;
    }

    public void setWords(String[] words) {
        this.words = words;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public Set<String> getSet() {
        return set;
    }

    public void setSet(Set<String> set) {
        this.set = set;
    }

    public HashMap<String, Integer> getMap() {
        return map;
    }

    public void setMap(HashMap<String, Integer> map) {
        this.map = map;
    }



    @Override
    public String getText() {
        System.out.print("Enter words: ");
        text = SCANNER.nextLine();
        return text;
    }

    @Override
    public String[] convertToArray() {
        return words = text.split(" ");
    }

    @Override
    public List<String> convertToList() {
        return list = Arrays.asList(words);
    }

    @Override
    public Set<String> convertToSet() {
        return set = new TreeSet<String>(list);
    }

    @Override
    public Map<String, Integer> convertToMap() {
        for(String entry: set) {
            int count = 0;
            for(String word: list) {
                if (entry.equals(word)){
                    count++;
                };
            }
            map.put(entry, count);
        }
        return map;
    }

    @Override
    public void findByWord() {
        System.out.print("Find word: ");
        String findWord = SCANNER.nextLine();
        if (set.contains(findWord)) {
            System.out.println("The word is find, count= " + map.get(findWord));
        } else {
            throw new NullPointerException("This word is absent!");
        }
    }
}
