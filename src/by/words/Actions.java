package by.words;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface Actions {
    String getText();
    String[] convertToArray();
    List<String> convertToList();
    Set<String> convertToSet();
    Map<String, Integer> convertToMap();
    void findByWord();
}
