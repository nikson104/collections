package rooms;

import java.util.ArrayList;
import java.util.List;

public class Building {
    private String name;
    private List<Room> rooms= new ArrayList<>();

    public Building(String name) {
        this.name = name;
    }

    public void addRoom(Room room){
        rooms.add(room);
    }

    public void information(){
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "Building{" +
                "name='" + name + '\'' +
                ", rooms=" + rooms +
                '}';
    }
}
