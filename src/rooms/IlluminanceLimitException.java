package rooms;

public class IlluminanceLimitException extends RuntimeException {

    public IlluminanceLimitException (String message){
        super(message);
    }
}
