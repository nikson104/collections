package rooms;

import java.util.ArrayList;
import java.util.List;

public class Room {
    List<Furniture> furnitures = new ArrayList<>();
    List<Lamp> lamps = new ArrayList<>();
    private int areaRoom;
    private int countWindows;
    private int totalArea;
    private int totalLight;

    public void addFurniture (Furniture furniture){
        if (totalArea>areaRoom*0.7){
            throw new SpaceLimitException("Space limit");
        }
        furnitures.add(furniture);
        totalArea+=furniture.getAreaFurniture();
    }

    public void addLamps(Lamp lamp){
        if ((totalLight+lamp.getLight())>4000){
            throw new IlluminanceLimitException("Illuminance limit");
        }
        lamps.add(lamp);
        totalLight+=lamp.getLight();
    }

    public Room(int areaRoom, int countWindows) {
        this.areaRoom = areaRoom;
        this.countWindows = countWindows;
        this.totalLight=countWindows*700;
    }

    @Override
    public String toString() {
        return "Room{" +
                "furnitures=" + furnitures +
                ", lamps=" + lamps +
                ", areaRoom=" + areaRoom +
                ", countWindows=" + countWindows +
                '}';
    }
}
