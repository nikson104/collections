package rooms;

public class Furniture {
    private int areaFurniture;
    private String name;

    public Furniture(String name, int areaFurniture) {
        this.areaFurniture = areaFurniture;
        this.name = name;
    }

    public int getAreaFurniture() {
        return areaFurniture;
    }

    public void setAreaFurniture(int areaFurniture) {
        this.areaFurniture = areaFurniture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Furniture{" +
                "areaFurniture=" + areaFurniture +
                ", name='" + name + '\'' +
                '}';
    }
}
