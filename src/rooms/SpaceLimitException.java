package rooms;

public class SpaceLimitException extends RuntimeException {

    public SpaceLimitException(String message) {
        super(message);
    }
}
