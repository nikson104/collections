package rooms;

public class Main {
    public static void main(String[] args) {
        Building building = new Building("Bulding 1");
        try {
            Room room = new Room(100, 2);
            building.addRoom(room);
            room.addFurniture(new Furniture("Table", 50));
            room.addFurniture(new Furniture("Sofa", 60));
            room.addLamps(new Lamp(400));
            room.addLamps(new Lamp(10000));
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
        building.information();
    }
}
