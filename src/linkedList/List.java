package linkedList;

public interface List<T> {

  boolean add(T element);
  void add(int index, T element);
  T get(int index);
  T set(int index, T e);
  void clear();
  boolean contains(Object element);
  T remove(int element);
}
