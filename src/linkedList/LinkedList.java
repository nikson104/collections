package linkedList;

import java.util.Arrays;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {

  private Node first;
  private Node last;
  private int size = 0;
  protected transient int modCount = 0;

  private static class Node<T> {
    T item;
    Node<T> next;
    Node<T> prev;

    Node(Node<T> prev, T element, Node<T> next) {
      this.item = element;
      this.next = next;
      this.prev = prev;
    }
  }

  public Iterator<T> iterator() {
    return listIterator();
  }

  public ListIterator<T> listIterator() {
    return listIterator(0);
  }

  public ListIterator<T> listIterator(final int index) {

    return new ListItr(index);
  }

  private class ListItr implements ListIterator<T> {
    private Node<T> lastReturned;
    private Node<T> next;
    private int nextIndex;
    private int expectedModCount = modCount;

    ListItr(int index) {
      next = (index == size) ? null : node(index);
      nextIndex = index;
    }

    public boolean hasNext() {
      return nextIndex < size;
    }

    public T next() {
      if (!hasNext())
        throw new NoSuchElementException();

      lastReturned = next;
      next = next.next;
      nextIndex++;
      return lastReturned.item;
    }

    public boolean hasPrevious() {
      return nextIndex > 0;
    }

    public T previous() {
      lastReturned = next = (next == null) ? last : next.prev;
      nextIndex--;
      return lastReturned.item;
    }

    public int nextIndex() {
      return nextIndex;
    }

    public int previousIndex() {
      return nextIndex - 1;
    }

    public void remove() {
      if (lastReturned == null)
        throw new IllegalStateException();

      Node<T> lastNext = lastReturned.next;
      unlink(lastReturned);
      if (next == lastReturned)
        next = lastNext;
      else
        nextIndex--;
      lastReturned = null;
      expectedModCount++;
    }

    public void set(T e) {
      if (lastReturned == null)
        throw new IllegalStateException();
      lastReturned.item = e;
    }

    public void add(T e) {
      lastReturned = null;
      if (next == null)
        linkLast(e);
      else
        linkBefore(e, next);
      nextIndex++;
      expectedModCount++;
    }


  }

  public T removeFirst() {
    final Node<T> f = first;
    if (f == null)
      throw new NoSuchElementException();
    return unlinkFirst(f);
  }

  public T removeLast() {
    final Node<T> l = last;
    if (l == null)
      throw new NoSuchElementException();
    return unlinkLast(l);
  }

  private T unlinkFirst(Node<T> f) {
    final T element = f.item;
    final Node<T> next = f.next;
    f.item = null;
    f.next = null; // help GC
    first = next;
    if (next == null)
      last = null;
    else
      next.prev = null;
    size--;
    modCount++;
    return element;
  }

  private T unlink(Node<T> x) {
    final T element = x.item;
    final Node<T> next = x.next;
    final Node<T> prev = x.prev;

    if (prev == null) {
      first = next;
    } else {
      prev.next = next;
      x.prev = null;
    }

    if (next == null) {
      last = prev;
    } else {
      next.prev = prev;
      x.next = null;
    }

    x.item = null;
    size--;
    modCount++;
    return element;
  }

  private T unlinkLast(Node<T> l) {
    final T element = l.item;
    final Node<T> prev = l.prev;
    l.item = null;
    l.prev = null; // help GC
    last = prev;
    if (prev == null)
      first = null;
    else
      prev.next = null;
    size--;
    modCount++;
    return element;
  }

  private void linkLast(T e) {
    final Node<T> l = last;
    final Node<T> newNode = new Node<>(l, e, null);
    last = newNode;
    if (l == null)
      first = newNode;
    else
      l.next = newNode;
    size++;
    modCount++;
  }

  private void linkFirst(T e) {
    final Node<T> f = first;
    final Node<T> newNode = new Node<>(null, e, f);
    first = newNode;
    if (f == null)
      last = newNode;
    else
      f.prev = newNode;
    size++;
    modCount++;
  }

  @Override
  public boolean add(T e) {
    linkLast(e);
    return true;
  }

  @Override
  public T get(int index) {
    return node(index).item;
  }

  private Node<T> node(int index) {
    if (index < (size >> 1)) {
      Node<T> x = first;
      for (int i = 0; i < index; i++)
        x = x.next;
      return x;
    } else {
      Node<T> x = last;
      for (int i = size - 1; i > index; i--)
        x = x.prev;
      return x;
    }
  }

  @Override
  public void clear() {
    for (Node<T> x = first; x != null; ) {
      Node<T> next = x.next;
      x.item = null;
      x.next = null;
      x.prev = null;
      x = next;
    }
    first = last = null;
    size = 0;
    modCount++;
  }

  @Override
  public boolean contains(Object element) {
    return indexOf(element) != -1;
  }

  public int indexOf(Object o) {
    int index = 0;
    if (o == null) {
      for (Node<T> x = first; x != null; x = x.next) {
        if (x.item == null)
          return index;
        index++;
      }
    } else {
      for (Node<T> x = first; x != null; x = x.next) {
        if (o.equals(x.item))
          return index;
        index++;
      }
    }
    return -1;
  }


  private void linkBefore(T e, Node<T> succ) {
    final Node<T> pred = succ.prev;
    final Node<T> newNode = new Node<>(pred, e, succ);
    succ.prev = newNode;
    if (pred == null)
      first = newNode;
    else
      pred.next = newNode;
    size++;
    modCount++;
  }

  public void add(int index, T element) {

    if (index == size)
      linkLast(element);
    else
      linkBefore(element, node(index));
  }

  public T remove(int index) {
    return unlink(node(index));
  }

  public T set(int index, T element) {
    Node<T> x = node(index);
    T oldVal = x.item;
    x.item = element;
    return oldVal;
  }

  public int size() {
    return size;
  }

  public Object[] toArray() {
    Object[] result = new Object[size];
    int i = 0;
    for (Node<T> x = first; x != null; x = x.next)
      result[i++] = x.item;
    return result;
  }


  @Override
  public String toString() {
    return "LinkedList{" +
        "list=" + Arrays.toString(this.toArray()) +
        ", size=" + size +
        '}';
  }
}