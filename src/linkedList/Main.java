package linkedList;

import java.util.Iterator;

public class Main {
  public static void main(String[] args) {
    LinkedList linkedList = new LinkedList();
    linkedList.add(2);
    linkedList.add(5);
    linkedList.add(6);
    linkedList.add(9);
    linkedList.add(10);
    System.out.println(linkedList);
    System.out.println(linkedList.get(1));
    System.out.println(linkedList.contains(3));
    System.out.println(linkedList.contains(5));
    linkedList.remove(1);
    System.out.println(linkedList);
    Iterator it = linkedList.iterator();
    while (it.hasNext()) {
      Integer obj = (Integer) it.next();
      if (obj.equals(new Integer(6))) {
        it.remove();
      }
      System.out.println(obj);
    }
    System.out.println(linkedList);

    linkedList.clear();
    System.out.println(linkedList);
  }
}