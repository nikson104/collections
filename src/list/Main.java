package list;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            arrayList.add(i);
        }
        System.out.println(arrayList);
        arrayList.size();
        arrayList.get(0);
        System.out.println(arrayList.get(0));
        arrayList.remove(3);
        System.out.println(arrayList);

        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Integer obj = (Integer) it.next();
            if (obj.equals(new Integer(6))) {
                it.remove();
            }
            System.out.println(obj);
        }
        System.out.println(arrayList);
    }
}