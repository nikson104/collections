package list;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

public class ArrayList<T> implements List<T>, Iterable<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private T[] array;
    private int index = 0;
    private int size;

    public ArrayList() {
        this.array = (T[]) new Object[DEFAULT_CAPACITY];
    }

    @Override
    public void add(T element) {
        if (index >= array.length) {
            T[] newArray = (T[]) new Object[array.length + DEFAULT_CAPACITY];
            System.arraycopy(array, 0, newArray, 0, array.length);
            array = newArray;
        }
        array[index] = element;
        index++;
        size++;
    }

    @Override
    public T get(int index) {
        return (T) array[index];

    }

    @Override
    public T remove(int index) {
        T oldValue = array[index];
        int value = size - index - 1;
        if (value > 0) {
            System.arraycopy(array, index + 1, array, index, value);
        }
        array[--size] = null;
        return oldValue;
    }


    @Override
    public int size() {
        return size;

    }


    @Override
    public String toString() {
        return "ArrayList { " +
                "array = " + Arrays.toString(array) +
                ", index = " + index +
                ", size = " + size +
                '}';
    }

    @Override
    public Iterator<T> iterator() {
        return new Itr();
    }

    private class Itr implements Iterator<T> {
        int cursor;
        int lastRet = -1;
        int expectedIndex = index;

        Itr() {}

        public boolean hasNext() {
            return cursor != size;
        }

        @SuppressWarnings("unchecked")
        public T next() {
            checkForComodification();
            int i = cursor;
            if (i >= size)
                throw new NoSuchElementException();
            Object[] elementData = ArrayList.this.array;
            if (i >= elementData.length)
                throw new ConcurrentModificationException();
            cursor = i + 1;
            return (T) elementData[lastRet = i];
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();
            checkForComodification();

            try {
                ArrayList.this.remove(lastRet);
                cursor = lastRet;
                lastRet = -1;
                expectedIndex = index;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }


        @SuppressWarnings("unchecked")
        public void forEachRemaining(Consumer<? super T> consumer) {
            Objects.requireNonNull(consumer);
            final int size = ArrayList.this.size;
            int i = cursor;
            if (i >= size) {
                return;
            }
            final Object[] elementData = ArrayList.this.array;
            if (i >= elementData.length) {
                throw new ConcurrentModificationException();
            }
            while (i != size && index == expectedIndex) {
                consumer.accept((T) elementData[i++]);
            }
            cursor = i;
            lastRet = i - 1;
            checkForComodification();
        }

        final void checkForComodification() {
            if (index != expectedIndex)
                throw new ConcurrentModificationException();
        }
    }
}